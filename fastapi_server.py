from fastapi import FastAPI, WebSocket
import asyncio
import glob
from scipy.optimize import linear_sum_assignment
import motmetrics as mm
from sklearn.metrics import pairwise_distances
import numpy as np
from simple_tracker import SimpleTracker, dist_cost_function, iou_cost_function
from strong_tracker import StrongTracker
from PIL import Image
import requests
from io import BytesIO
import re
import base64
import os

from dis_track_5_0_25 import track_data, country_balls_amount
#from simple_track_1 import track_data, country_balls_amount


app = FastAPI(title='Tracker assignment')
imgs = glob.glob('imgs/*')
country_balls = [{'cb_id': x, 'img': imgs[x % len(imgs)]} for x in range(country_balls_amount)]
print('Started')

track_list = []

def tracker_soft(el, simple_tracker):
    """
    Необходимо изменить у каждого словаря в списке значение поля 'track_id' так,
    чтобы как можно более длительный период времени 'track_id' соответствовал
    одному и тому же кантри болу.

    Исходные данные: координаты рамки объектов

    Ограничения:
    - необходимо использовать как можно меньше ресурсов (представьте, что
    вы используете embedded устройство, например Raspberri Pi 2/3).
    -значение по ключу 'cb_id' является служебным, служит для подсчета метрик качества
    вашего трекера, использовать его в алгоритме трекера запрещено
    - запрещается присваивать один и тот же track_id разным объектам на одном фрейме
    """
    simple_tracker.update(el)
    print("-----------------------------")
    print(el)
    return el


def tracker_strong(el, strong_tracker):
    """
    Необходимо изменить у каждого словаря в списке значение поля 'track_id' так,
    чтобы как можно более длительный период времени 'track_id' соответствовал
    одному и тому же кантри болу.

    Исходные данные: координаты рамки объектов, скриншоты прогона

    Ограничения:
    - вы можете использовать любые доступные подходы, за исключением
    откровенно читерных, как например захардкодить заранее правильные значения
    'track_id' и т.п.
    - значение по ключу 'cb_id' является служебным, служит для подсчета метрик качества
    вашего трекера, использовать его в алгоритме трекера запрещено
    - запрещается присваивать один и тот же track_id разным объектам на одном фрейме

    P.S.: если вам нужны сами фреймы, измените в index.html значение make_screenshot
    на true для первого прогона, на повторном прогоне можете читать фреймы из папки
    и по координатам вырезать необходимые регионы.
    TODO: Ужасный костыль, на следующий поток поправить
    """
    strong_tracker.update(el)
    print("-----------------------------")
    print(el)
    return el


def calculate_metrics(track_list):
    print("******** Metrics ***********")
    # print(track_list)
    acc = mm.MOTAccumulator(auto_id=True)
    for track in track_list:
        track_ids = [x["track_id"] for x in track["data"] if x["track_id"] is not None]
        gt_ids = [x["cb_id"] for x in track["data"]]
        target_positions = np.array([(x["x"], x["y"]) for x in track["data"]])
        pred_positions = target_positions[[x["track_id"] is not None for x in track["data"]]]
        if len(pred_positions) == 0:
            dists = []
        else:
            dists = pairwise_distances(target_positions, pred_positions).tolist()
        # print(gt_ids, track_ids, dists)
        acc.update(
            gt_ids,
            track_ids,
            dists,
        )
    #print(acc.events)
    mh = mm.metrics.create()
    summary = mh.compute(acc, metrics=["num_frames", "mota"], name="acc")
    print(summary)


@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    #make_screenshots = False
    print('Accepting client connection...')
    await websocket.accept()
    # отправка служебной информации для инициализации объектов
    # класса CountryBall на фронте
    await websocket.send_text(str(country_balls))

    # iou
    # simple_tracker = SimpleTracker(threshold=0.002, func=iou_cost_function, maximize=True, hist_time=0)

    # dist
    # simple_tracker = SimpleTracker(threshold=50000, func=dist_cost_function, maximize=False, hist_time=0)

    # dist with hist
    simple_tracker = SimpleTracker(threshold=50000, func=dist_cost_function, maximize=False, hist_time=5)

    # strong_tracker = StrongTracker(image_dir="dis_track_5_0_25", threshold=(40000), hist_time=5)

    for el in track_data:
        await asyncio.sleep(0.5)
            
        el = tracker_soft(el, simple_tracker)
        # el = tracker_strong(el, strong_tracker)
        track_list.append(el)

        await websocket.send_json(el)

    calculate_metrics(track_list)
    print('Bye..')



# @app.websocket("/ws")
# async def websocket_endpoint(websocket: WebSocket):
#     print('Accepting client connection...')
#     await websocket.accept()
#     await websocket.receive_text()
#     # отправка служебной информации для инициализации объектов
#     # класса CountryBall на фронте

#     dir = "dis_track_5_0_25"
#     if not os.path.exists(dir):
#         os.makedirs(dir)

#     await websocket.send_text(str(country_balls))
#     for el in track_data:
#         await asyncio.sleep(0.5)
#         image_data = await websocket.receive_text()
#         # print(image_data)
#         try:
#             image_data = re.sub('^data:image/.+;base64,', '', image_data)
#             image = Image.open(BytesIO(base64.b64decode(image_data)))
#             image = image.resize((1000, 800), Image.ANTIALIAS)
#             image.save(f"{dir}/frame_{el['frame_id']}.png")
#             print(image)
#         except Exception as e:
#             print(e)
    
#         # отправка информации по фрейму
#         await websocket.send_json(el)

#     await websocket.send_json(el)
#     await asyncio.sleep(0.5)
#     image_data = await websocket.receive_text()
#     # print(image_data)
#     try:
#         image_data = re.sub('^data:image/.+;base64,', '', image_data)
#         image = Image.open(BytesIO(base64.b64decode(image_data)))
#         image = image.resize((1000, 800), Image.ANTIALIAS)
#         image.save(f"{dir}/frame_{el['frame_id'] + 1}.png")
#         print(image)
#     except Exception as e:
#         print(e)

#     print('Bye..')