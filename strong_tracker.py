from scipy.optimize import linear_sum_assignment
import numpy as np
import copy
import cv2


def dist_cost_function(obj1, obj2):
    return (obj1["x"] - obj2["x"]) ** 2 + (obj1["y"] - obj2["y"]) ** 2


def fix_bbox(bbox):
    return (
        bbox[0] + 59,
        bbox[1] + 99,
        bbox[2] + 59,
        bbox[3] + 99,
    )


def normalize_bbox(bbox, size):
    return [
        max(0, min(bbox[0], size[1])),
        max(0, min(bbox[1], size[0])),
        max(0, min(bbox[2], size[1])),
        max(0, min(bbox[3], size[0])),
    ]


def cost_function_with_image(curr_obj, prev_obj, current_frame, prev_frame, image_folder):
    
    current_image = cv2.imread(image_folder + '/frame_' + str(current_frame + 1) + '.png')
    prev_image = cv2.imread(image_folder + '/frame_' + str(prev_frame + 1) + '.png')
    
    prev_box = prev_obj["bounding_box"]

    curr_box = curr_obj["bounding_box"]
    curr_box = fix_bbox(curr_box)
    curr_box = normalize_bbox(curr_box, current_image.shape)

    prev_box = prev_obj["bounding_box"]
    prev_box = fix_bbox(prev_box)
    prev_box = normalize_bbox(prev_box, prev_image.shape)

    # print(image_folder + '/frame_' + str(current_frame) + '.png')
    # print(current_image.shape)
    # print(curr_box)
    
    curr_obj_cropped = current_image[curr_box[1]:curr_box[3], curr_box[0]:curr_box[2]]
    prev_obj_cropped = prev_image[prev_box[1]:prev_box[3], prev_box[0]:prev_box[2]]

    if curr_obj_cropped.size == 0 or prev_obj_cropped.size == 0:
        image_dist = 10000000
    else:
        # print(curr_obj_cropped.shape)
        # cv2.imwrite(f"test_{current_frame}.png", curr_obj_cropped)
        curr_obj_resized = cv2.resize(curr_obj_cropped, (10, 10))
        prev_obj_resized = cv2.resize(prev_obj_cropped, (10, 10))

        image_dist = ((curr_obj_resized - prev_obj_resized) ** 2).sum()

    return image_dist #dist_cost_function(curr_obj, prev_obj) + image_dist


class StrongTracker:

    def __init__(self, image_dir, threshold=None, hist_time=0):
        self.previous_objects = None
        self.previous_frame = None
        self.max_track = 0
        self.threshold = threshold
        self.hist_time = hist_time
        self.image_dir = image_dir

    # write track to real objects
    def add_new_track(self, objects, indexes):
        for row_i in indexes:
            objects[row_i][1]["track_id"] = self.max_track
            self.max_track += 1

    # matches real objects with all
    def match_real(self, current_objects, real_objects):
        for i in range(len(current_objects)):
            for obj in real_objects:
                if i == obj[0]:
                    current_objects[i] = obj[1]
                    break

    def update(self, el):

        current_frame = el["frame_id"]
    
        # real objects - objects with bounding box
        # 0 idx -to match objects, 2 idx - for history
        real_objects = []
        for i, obj in enumerate(el["data"]):
            if obj["bounding_box"]:
                real_objects.append([i, obj, 0])

        if len(real_objects) == 0:
            # if previous_objects is not None:
            #     previous_objects = real_objects
            return
        
        if self.previous_objects is None:
            self.previous_objects = real_objects
            self.add_new_track(self.previous_objects, range(len(self.previous_objects)))
            self.match_real(el["data"], self.previous_objects)
            self.previous_frame = el["frame_id"]
            # print("Init: ", el)
            # print("Init: ", self.previous_objects)
            return

        cost_matrix = np.zeros((len(real_objects), len(self.previous_objects)))
        for i, current_object in enumerate(real_objects):
            for j, previous_object in enumerate(self.previous_objects):
                cost_matrix[i, j] = cost_function_with_image(
                    current_object[1], previous_object[1], current_frame, self.previous_frame, self.image_dir
                )

        #print("cost matrix: ", cost_matrix)
        row_ind, col_ind = linear_sum_assignment(cost_matrix)

        new_dets = set(range(0, len(real_objects))) - set(row_ind)

        # if with threshold - assign as new objects those which cost less then threshold
        if self.threshold:
            mask = cost_matrix[row_ind, col_ind] > self.threshold
            new_dets.update(row_ind[mask])
            for row_i, col_i in zip(row_ind[~mask], col_ind[~mask]):
                real_objects[row_i][1]["track_id"] = self.previous_objects[col_i][1]["track_id"]
        else:
            for row_i, col_i in zip(row_ind, col_ind):
                real_objects[row_i][1]["track_id"] = self.previous_objects[col_i][1]["track_id"]
        
        self.add_new_track(real_objects, new_dets)
        self.match_real(el["data"], real_objects)


        # print("Real: ", real_objects)
        # print("Prev: ", self.previous_objects)
        new_previous_objects = real_objects
        for i in range(len(self.previous_objects)):
            for j in range(len(real_objects)):
                if real_objects[j][1]["track_id"] == self.previous_objects[i][1]["track_id"]:
                    break
            else:
                if self.previous_objects[i][2] < self.hist_time:
                    self.previous_objects[i][2] += 1
                    new_previous_objects.append(self.previous_objects[i])
        # print("New prev: ", new_previous_objects)

        self.previous_objects = new_previous_objects
        self.previous_frame = el["frame_id"]


        