from scipy.optimize import linear_sum_assignment
import numpy as np
import copy


def dist_cost_function(obj1, obj2):
    return (obj1["x"] - obj2["x"]) ** 2 + (obj1["y"] - obj2["y"]) ** 2


def iou_cost_function(obj1, obj2):
    boxA = obj1["bounding_box"]
    boxB = obj2["bounding_box"]
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])
    interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)
    boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
    boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)
    return interArea / float(boxAArea + boxBArea - interArea)


class SimpleTracker:

    def __init__(self, func=dist_cost_function, maximize=False, threshold=None, hist_time=0):
        self.previous_objects = None
        self.max_track = 0
        self.current_cost_function = func
        self.maximize = maximize
        self.threshold = threshold
        self.hist_time = hist_time

    # write track to real objects
    def add_new_track(self, objects, indexes):
        for row_i in indexes:
            objects[row_i][1]["track_id"] = self.max_track
            self.max_track += 1

    # matches real objects with all
    def match_real(self, current_objects, real_objects):
        for i in range(len(current_objects)):
            for obj in real_objects:
                if i == obj[0]:
                    current_objects[i] = obj[1]
                    break

    def update_hist(self, real_objects):
        # print("Real: ", real_objects)
        # print("Prev: ", self.previous_objects)
        new_previous_objects = real_objects
        for i in range(len(self.previous_objects)):
            for j in range(len(real_objects)):
                if real_objects[j][1]["track_id"] == self.previous_objects[i][1]["track_id"]:
                    break
            else:
                if self.previous_objects[i][2] < self.hist_time:
                    self.previous_objects[i][2] += 1
                    new_previous_objects.append(self.previous_objects[i])
        # print("New prev: ", new_previous_objects)
        return new_previous_objects

    def update(self, el):
    
        # real objects - objects with bounding box
        # 0 idx -to match objects, 2 idx - for history
        real_objects = []
        for i, obj in enumerate(el["data"]):
            if obj["bounding_box"]:
                real_objects.append([i, obj, 0])

        if len(real_objects) == 0:
            if self.previous_objects is not None:
                self.previous_objects = self.update_hist(real_objects)
            return
        
        if self.previous_objects is None:
            self.previous_objects = real_objects
            self.add_new_track(self.previous_objects, range(len(self.previous_objects)))
            self.match_real(el["data"], self.previous_objects)
            print("Init: ", el)
            return

        cost_matrix = np.zeros((len(real_objects), len(self.previous_objects)))
        for i, current_object in enumerate(real_objects):
            for j, previous_object in enumerate(self.previous_objects):
                cost_matrix[i, j] = self.current_cost_function(current_object[1], previous_object[1])

        #print("Cost matrix: ", cost_matrix)
        row_ind, col_ind = linear_sum_assignment(cost_matrix, maximize=self.maximize)

        new_dets = set(range(0, len(real_objects))) - set(row_ind)

        # if with threshold - assign as new objects those which cost less then threshold
        if self.threshold:
            if self.maximize:
                mask = cost_matrix[row_ind, col_ind] < self.threshold
            else:
                mask = cost_matrix[row_ind, col_ind] > self.threshold
            new_dets.update(row_ind[mask])
            for row_i, col_i in zip(row_ind[~mask], col_ind[~mask]):
                real_objects[row_i][1]["track_id"] = self.previous_objects[col_i][1]["track_id"]
        else:
            for row_i, col_i in zip(row_ind, col_ind):
                real_objects[row_i][1]["track_id"] = self.previous_objects[col_i][1]["track_id"]
        
        self.add_new_track(real_objects, new_dets)
        self.match_real(el["data"], real_objects)

        self.previous_objects = self.update_hist(real_objects)


        